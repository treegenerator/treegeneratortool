# Tree Generator Tool (console)

## Dependencies 
included in /libs:  
gson-2.8.2.jar  
jgit-4.10.0.201712302008-r.jar  
tools.jar  
AnalysisUtil.jar (part of research thesis)  

## Build tool
ant (or Eclipse or similar IDE)

## Quickstart

1. Navigate to sources folder
2. Execute to get source code:
```
git clone git@gitlab.com:treegenerator/treegeneratortool.git
```

3. Execute to build .jar file:

```
ant -buildfile build/build.xml
```

4. Execute to run tool:

```
java -jar build/TreeGeneratorTool.jar
```

## About
This tool is based on the work included in the published paper [*"Generating Trees for Comparison"*](https://doi.org/10.3390/computers9020035) 
* by Danijel Mlinaric, Vedran Mornar and Boris Milasinovic
* Faculty of Electrical Engineering and Computing, University of Zagreb, Croatia, 2020

Source code of the console tool used in the paper and web tool can be found [here](https://gitlab.com/treegenerator). Tree generator web tool page is available [here](https://treegenerator.herokuapp.com).
						
Three algorithms for generating trees for comparison are presented and evaluated:

| Algorithm       | Description          |
| ------------- |-------------|
| Random      | Various variants of random algorithm to generate trees are described in paper, in this tool random parent and child selection with unused set is used (*Random P-C with Un*). Algorithm as input accepts number of nodes where the connections between nodes are randomly created. | 
| Pattern     | Trees created by data from various domains conform to the certain pattern in the nodes distribution per level. Algorithm as input accepts number of nodes, levels and nodes distribution per level in percentage. Output is a random tree that conform to the required pattern.     |  
| Distort | In many research areas trees are compared, where compared trees are similar. Distortion algorithm provides distortion of the input trees by the number of added, deleted nodes and percentage of matched nodes with changed parent.   | 

Work on this tool is part of a program analysis and software evolution research in the area of Dynamic Software Updating (DSU) as part of the PhD research "Extending dynamic software update model to class hierachy changes and run-time phenomena detection" by PhD candidate Danijel Mlinaric under supervision of Prof. Boris Milasinovic.