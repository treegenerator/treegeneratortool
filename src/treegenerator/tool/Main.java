package treegenerator.tool;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import classhier.AStarSearchDiff;
import classhier.AbstractGraphDiff;
import classhier.AbstractGraphDiff.DiffResult;
import classhier.EditGraphDiff;
import classhier.EvolutionAnalysis;
import classhier.graph.Graph;
import classhier.graph.GraphImportExport;
import classhier.graph.Tree;
import classhier.graph.TreeStatistics;
import javafx.util.Pair;
import treegenerator.algorithms.GenParams;
import treegenerator.algorithms.GenerateRandom;
import treegenerator.algorithms.TreeComparisonGen;
import util.ValueStatistics;
import util.github.GithubHelper;
import util.github.Repository;

public class Main {
	public String releasesFolder = "C:\\Temp";
	
	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
		System.out.println("TreeGeneratorTool v1");
		System.out.println("Analysis setup from paper \"Generating Trees For Comparison\" (2020)");
		System.out.println("Authors: Danijel Mlinaric, Vedran Mornar, Boris Milasinovic");
		System.out.println();
		TreeComparisonGen tdg = new TreeComparisonGen();
		Scanner in = new Scanner(System.in);
		int selection = 1;
		
		if(args.length > 1 && args[0] == "o") {
			selection = Integer.parseInt(args[1]);
		} else {
			System.out.println("Options:");
			System.out.println("1 - Random algorithm variants tree heights and Equal Hits (EH)");
			System.out.println("2 - Node distribution in program versions");
			System.out.println("3 - Experiment 1: Statistical and edit distance measurements");
			System.out.println("4 - Experiment 2: Distortion parameters");
			System.out.println("5 - Experiment 3: Node distribution");
			System.out.println("Enter option:");
			
			selection = in.nextInt();
		}
		
		if(selection == 1) {
			System.out.println("**** Random algorithm variants tree heights and Equal Hits (EH) ****");
			System.out.println("Show raw data Y/N:");
			boolean showData = in.nextLine().toUpperCase().contains("Y") ? true : false;
			ValueStatistics depth = new ValueStatistics();
			ValueStatistics eh = new ValueStatistics();
			
			GenerateRandom gr = new GenerateRandom(new GenParams(100));

			System.out.println("* Random Parent - Child (random P-C) *");
			for(int i = 0; i < 10000; i++) {
				Tree t = gr.generateRandomPC();
				t.calculateStatistics();
				int d = t.getStats().getDepth();
				depth.addValue(d);
				eh.addValue(gr.getEqualsHit());
				if(showData) {
					System.out.println(String.format("Tree %d depth:\t%d", i + 1,  d));
				}
			}
			depth.calcStats(); eh.calcStats();
			System.out.println("avg depth: " + depth);
			System.out.println("avg eh: " + eh);
			System.out.println();
			
			gr.resetEqualsHit(); depth = new ValueStatistics(); eh = new ValueStatistics();
			System.out.println("* Random Parent - Child with Unused set(random P-C with Un) *");
			for(int i = 0; i < 10000; i++) {
				Tree t = gr.generateTreePCUn();
				t.calculateStatistics();
				int d = t.getStats().getDepth();
				depth.addValue(d);
				eh.addValue(gr.getEqualsHit());
				if(showData) {
					System.out.println(String.format("Tree %d depth:\t%d", i + 1,  d));
				}
			}
			depth.calcStats(); eh.calcStats();
			System.out.println("avg depth: " + depth);
			System.out.println("avg eh: " + eh);
			System.out.println();
			
			gr.resetEqualsHit(); depth = new ValueStatistics(); eh = new ValueStatistics();
			System.out.println("* Random Parent iterative Child (random P iterative C) *");
			for(int i = 0; i < 10000; i++) {
				Tree t = gr.generateTreeIterChild();
				t.calculateStatistics();
				int d = t.getStats().getDepth();
				depth.addValue(d);
				eh.addValue(gr.getEqualsHit());
				if(showData) {
					System.out.println(String.format("Tree %d depth:\t%d", i + 1,  d));
				}
			}
			depth.calcStats(); eh.calcStats();
			System.out.println("avg depth: " + depth);
			System.out.println("avg eh: " + eh);
			System.out.println();
			
			gr.resetEqualsHit(); depth = new ValueStatistics(); eh = new ValueStatistics();
			System.out.println("* Random Parent - Child with Unused and Used set (random P - C with Un/Us) *");
			for(int i = 0; i < 10000; i++) {
				Tree t = gr.generateTree1_RandomUsedUnused();
				t.calculateStatistics();
				int d = t.getStats().getDepth();
				depth.addValue(d);
				eh.addValue(gr.getEqualsHit());
				if(showData) {
					System.out.println(String.format("Tree %d depth:\t%d", i + 1,  d));
				}
			}
			depth.calcStats(); eh.calcStats();
			System.out.println("avg depth: " + depth);
			System.out.println("avg eh: " + eh);
			System.out.println();
			
		}else if(selection == 2) { 
			System.out.println("**** Node distribution in program versions ****");
			
			EvolutionAnalysis ea = new EvolutionAnalysis();
			Repository [] reps = {
					GithubHelper.getRepository("TeamNewPipe", "NewPipe"),
					GithubHelper.getRepository("naver", "pinpoint"),
					GithubHelper.getRepository("facebook", "litho"),
					GithubHelper.getRepository("neo4j", "neo4j"),
					GithubHelper.getRepository("seata", "seata")
			};
			/*
			String[] programPaths = new String[] {
					"C:\\Temp\\NewPipe\\versions",
					"C:\\Temp\\litho\\versions",
					"C:\\Temp\\pinpoint\\versions",
					"C:\\Temp\\neo4j\\versions",
					"C:\\Temp\\litho\\versions",
					"C:\\Temp\\seata\\versions"
			};
			
			for(String progP : programPaths) {
				System.out.println(String.format("Analyzing from: %s", progP));
			*/
			for(Repository r : reps) {
				System.out.println(String.format("Analyzing from: %s", r.getName()));
				
				ea.analyzeFromGit(r);
				ea.analyzeHierarchyDiffSubsequent();
				
				System.out.println("version\tn\td\tNd\tDa\tdist");
				for(Map.Entry<String, Graph> e : ea.getSubsequentTrees().entrySet()) {
					System.out.print(String.format("%s: ", e.getKey()));
					Tree t = (Tree)e.getValue();
					t.calculateStatistics();
					TreeStatistics ts = t.getStats();

					System.out.print(String.format("%d\t%d\t%s\t%s\t%s\n", t.getSize(), ts.getDepth(),
							ts.getNodesPerDepthShort(), ts.getDegreePerDepthShort(),
							ts.getDistString(false)
							));
				}
			}
		}else if(selection == 3) {
			System.out.println("**** Experiment 1: Statistical and edit distance measurements ****");
			System.out.println("---- Tree1: random (10 nodes), Tree2: distorted (1, 1, 0.5) ----");
			//distance values for A* alg and tree ordered alg.
			//results shown for the first 10 runs, statistics are for all 30 runs
			int n = 30;

			//test
			HashMap<Integer, ValueStatistics> nstat1 = new HashMap<Integer, ValueStatistics>(),
					nstat2 = new HashMap<Integer, ValueStatistics>();
			HashMap<Integer, ValueStatistics> dstat1 = new HashMap<Integer, ValueStatistics>(),
					dstat2 = new HashMap<Integer, ValueStatistics>();
			ValueStatistics gedstat = new ValueStatistics(),
					tedstat = new ValueStatistics();

			GenParams gp = new GenParams(10, 1, 1, 0.5f);
			System.out.println("Algorithm: " + tdg.getClass().getName());
			System.out.println("Generate params: " + gp);
			System.out.println("Number of iterations: " + n);
			System.out.println("nodesPerDepth1 |\tnodesPerDepth2|\tavgDegPerDep1|\tavgDegPerDep2|\tA* dist|\tord. dist");
			
			ExecutorService executor = Executors.newSingleThreadExecutor();
			for(int i = 0; i < n; i++) {
				Pair<Tree, Tree> gen = null;
				try {
					gen = tdg.generateRandomDistort(gp);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Tree t1 = gen.getKey(), t2 = gen.getValue();
				
				t1.calculateStatistics(); t2.calculateStatistics();
				TreeStatistics ts1 = t1.getStats(), ts2 = t2.getStats();
				
				EditGraphDiff egd = new EditGraphDiff(t1, t2);
				egd.calculate();
				
		        Future<Integer> future = executor.submit(new AStarTask(t1, t2));
			    int AStarDist = -1;
		        
			    try {
			    	AStarDist = future.get(3, TimeUnit.MINUTES);
			    } catch(TimeoutException e) {
			    	System.out.println("A* algorithm timeouted after 3 minutes");
			    	future.cancel(true);
			    	i--;
			    	continue;	
			    } 
			    
			    //add to stats
			    for(Map.Entry<Integer, Integer> e : ts1.getNumberOfNodesPerDepth().entrySet()) {
			    	if(!nstat1.containsKey(e.getKey())) {
			    		nstat1.put(e.getKey(), new ValueStatistics());
			    	}
			    	nstat1.get(e.getKey()).addValue(e.getValue());
			    }
			    for(Map.Entry<Integer, Integer> e : ts2.getNumberOfNodesPerDepth().entrySet()) {
			    	if(!nstat2.containsKey(e.getKey())) {
			    		nstat2.put(e.getKey(), new ValueStatistics());
			    	}
			    	nstat2.get(e.getKey()).addValue(e.getValue());
			    }
			    
			    for(Map.Entry<Integer, ValueStatistics> e : ts1.getdegreeStatsPerDepth().entrySet()) {
			    	if(!dstat1.containsKey(e.getKey())) {
			    		dstat1.put(e.getKey(), new ValueStatistics());
			    	}
			    	dstat1.get(e.getKey()).addAll(e.getValue().getValues());
			    }
			    for(Map.Entry<Integer, ValueStatistics> e : ts2.getdegreeStatsPerDepth().entrySet()) {
			    	if(!dstat2.containsKey(e.getKey())) {
			    		dstat2.put(e.getKey(), new ValueStatistics());
			    	}
			    	dstat2.get(e.getKey()).addAll(e.getValue().getValues());
			    }
			    
			    int tedres = Math.round((float)(((DiffResult)egd.getDiff().get(0)).getResult()));
			    
			    tedstat.addValue(tedres);
			    gedstat.addValue(AStarDist);
				
				System.out.println(String.format("%s\t%s\t%s\t%s\t%d\t%d", 
						ts1.getNodesPerDepthShort(), ts2.getNodesPerDepthShort(),
						ts1.getDegreePerDepthShort(), ts2.getDegreePerDepthShort(),
						AStarDist, 
						tedres)
						);
			}
			executor.shutdownNow();
			//average
			System.out.println("average:");
			for(ValueStatistics vs : nstat1.values()) {
		    	vs.calcStats();
		    }
			for(ValueStatistics vs : nstat2.values()) {
		    	vs.calcStats();
		    }
			for(ValueStatistics vs : dstat1.values()) {
		    	vs.calcStats();
		    }
			for(ValueStatistics vs : dstat2.values()) {
		    	vs.calcStats();
		    }
			gedstat.calcStats(); tedstat.calcStats();
			//end average
			
			System.out.println(String.format("%s\t%s\t%s\t%s\t%s\t%s", 
					ValueStatistics.getStatsPerElementShort(nstat1),ValueStatistics.getStatsPerElementShort(nstat2),
					ValueStatistics.getStatsPerElementShort(dstat1),ValueStatistics.getStatsPerElementShort(dstat2),
					gedstat.getMeanStdDev(), 
					tedstat.getMeanStdDev())
					);
		    
		} else if(selection == 4) {
				System.out.println("**** Experiment 2: Distortion parameters ****");
				System.out.println("---- Tree1: random (10 nodes), Tree2: distorted (1, 1, 0.5) ----");
				//100 iterations
				int n = 100;
				GenParams[] p = new GenParams[] {
						//posebno testiranje rasta obrisanih -> jer je cudno usporediti razlicit gen par. #1 i #2
						new GenParams(100, 0, 10, 0),
						new GenParams(100, 0, 20, 0),
						new GenParams(100, 0, 30, 0),
						new GenParams(100, 0, 40, 0),
						new GenParams(100, 0, 50, 0),
						new GenParams(100, 0, 60, 0),
						new GenParams(100, 0, 70, 0),
						new GenParams(100, 0, 80, 0),
						new GenParams(100, 0, 90, 0),
						
						//i dodanih, ali tu ne očekujem velika iznenađenja
						new GenParams(100, 10, 0, 0),
						new GenParams(100, 20, 0, 0),
						new GenParams(100, 30, 0, 0),
						new GenParams(100, 40, 0, 0),
						new GenParams(100, 50, 0, 0),
						new GenParams(100, 60, 0, 0),
						new GenParams(100, 70, 0, 0),
						new GenParams(100, 80, 0, 0),
						new GenParams(100, 90, 0, 0),
						
						//jos permutacije
						new GenParams(100, 0, 0, 0.1f),
						new GenParams(100, 0, 0, 0.2f),
						new GenParams(100, 0, 0, 0.3f),
						new GenParams(100, 0, 0, 0.4f),
						new GenParams(100, 0, 0, 0.5f),
						new GenParams(100, 0, 0, 0.6f),
						new GenParams(100, 0, 0, 0.7f),
						new GenParams(100, 0, 0, 0.8f),
						new GenParams(100, 0, 0, 0.9f),
						};
				
				System.out.println("Algorithm: " + tdg.getClass().getName());
				System.out.println("Number of iterations: " + n);
//				System.out.println("N1\tadded\tdeleted\tperPer\tdistance");
				System.out.println("N1\tadded\tdeleted\tperPer\tdistance\taddops\tdeleteops\tsubops");
				for(GenParams gp : p) {
//					System.out.println("Generate params: " + gp);
					
					ValueStatistics vs = new ValueStatistics();
					
					ValueStatistics vsAddOps = new ValueStatistics(),
							vsDelOps = new ValueStatistics(),
							vsSubOps = new ValueStatistics();
					for(int i = 0; i < n; i++) {
//						System.out.println("Iteration - " + (i + 1));
						
						Pair<Tree, Tree> gen = null;
						try {
							gen = tdg.generateRandomDistort(gp);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
//						System.out.println("Edit graph algorithm:");
						EditGraphDiff egd = new EditGraphDiff(gen.getKey(), gen.getValue());
						egd.calculate();
						
						//additionally show number of ops
						egd.getTransform();
						vsAddOps.addValue(egd.getAddedNodes().size());
						vsDelOps.addValue(egd.getDeletedNodes().size());
						vsSubOps.addValue(egd.getSubstitutedNodes().size());
						
						//
//						for(AbstractGraphDiff.DiffResult dr : egd.getDiff()) {
//							System.out.println(dr.toString());
//						}
//						System.out.println(egd.getTransform());
						
						vs.addValue(Math.round((float)(((DiffResult)egd.getDiff().get(0)).getResult())));
						
					}
					vs.calcStats();
					
					vsAddOps.calcStats(); vsDelOps.calcStats(); vsSubOps.calcStats();
//					System.out.println(String.format("%d\t%d\t%d\t%f\t%s",
					System.out.println(String.format("%d\t%d\t%d\t%f\t%s\t%s\t%s\t%s",
							gp.getSize(), gp.getAddedNodes(), gp.getDeletedNodes(), gp.getMatchedPermutation(),
							//vs.getMeanStdDev()));
							vs.getMeanStdDev(),
							vsAddOps.getMeanStdDev(),
							vsDelOps.getMeanStdDev(),
							vsSubOps.getMeanStdDev()));
//					System.out.println("Distance stats:");
//					System.out.println(vs);
					//za box wisker graf ispis vrijednosti
					System.out.println("Values:");
					System.out.println(vs.getValuesAsString());
			}
		} else if(selection == 5) {
			System.out.println("**** Experiment 3: Node distribution ****");
			System.out.println("---- Tree1: pattern (100 nodes), Tree2: distorted ----");
			float[][] distToTest = {
					{ 0.2277f, 0.4804f, 0.1605f, 0.0638f, 0.0374f, 0.0302f},
					{ 0.2f, 0.2f, 0.2f, 0.2f, 0.2f },
					{ 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f },
					{ 0.1f, 0.2f, 0.3f, 0.4f },
					{ 0.4f, 0.3f, 0.2f, 0.1f },
					{ 0.0625f, 0.125f, 0.1875f, 0.25f, 0.1875f, 0.125f, 0.0625f },
					{ 0.039f, 0.063f, 0.078f, 0.0875f, 0.0915f, 0.0935f, 0.095f,
				        0.0935f, 0.0915f, 0.0875f, 0.078f, 0.063f, 0.039f }
			};

			GenParams[] p = new GenParams[] { 
					new GenParams(100, 10, 10, 0.1f),
					new GenParams(100, 50, 0, 0),
					new GenParams(100, 0, 50, 0),
					new GenParams(100, 0, 0, 0.5f)
			};
			
			ExecutorService executor = Executors.newSingleThreadExecutor();
			for(GenParams gp : p) {
				System.out.println("Algorithm: " + tdg.getClass().getName());
				System.out.println("Generate params: " + gp);
				
				System.out.println("T1 dep - dist  |\t T2 dep - dist |\tA* dist|\tord. dist");
				
				for(float[] dist : distToTest) {
					Pair<Tree, Tree> gen = null;
					Tree t1 = null, t2 = null;
					int AStarDist = -1;
					
					gp.setDist(dist);
					
					if(gp.getSize() < 12) {
						do {
							try {
								gen = tdg.generatePatternDistort(gp);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								continue;
							}
							t1 = gen.getKey(); t2 = gen.getValue();
		
					        Future<Integer> future = executor.submit(new AStarTask(t1, t2));
						    try {
						    	AStarDist = future.get(5, TimeUnit.MINUTES);
						    } catch(TimeoutException e) {
						    	System.out.println("A* algorithm timeouted after 5 minutes");
						    	future.cancel(true);
						    } 	
						    
					    } while(AStarDist == -1);
					} else {
						try {
							gen = tdg.generatePatternDistort(gp);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							continue;
						}
						t1 = gen.getKey(); t2 = gen.getValue();
						try {
							GraphImportExport.exportToFile(t1, "E:\\test1.dot", false);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						try {
							GraphImportExport.exportToFile(t2, "E:\\test2.dot", false);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					
					t1.calculateStatistics(); t2.calculateStatistics();
					TreeStatistics ts1 = t1.getStats(), ts2 = t2.getStats();
					
					EditGraphDiff egd = new EditGraphDiff(t1, t2);
					egd.calculate();
					
					System.out.println(String.format("%d - %s |\t%d - %s |\t%d\t%d", 
							ts1.getDepth(), ts1.getDistString(false), 
							ts2.getDepth(), ts2.getDistString(false),
							AStarDist,
							Math.round((float)(((DiffResult)egd.getDiff().get(0)).getResult()))	//grdo mi je to, trebalo bi nekako prebaciti u egd
							));
				}
			}
			executor.shutdownNow();
		    System.out.println("end");
		
		} 
	    return;

	}
	
}

class AStarTask implements Callable<Integer> {
	Tree t1, t2;
	public AStarTask(Tree t1, Tree t2) {
		this.t1 = t1;
		this.t2 = t2;
	}
	
	@Override
	public Integer call() throws Exception {
		AStarSearchDiff gd = new AStarSearchDiff(t1, t2);
    	gd.calculate();
		return (int)(((DiffResult)gd.getDiff().get(0)).getResult());
	}

}

class Task implements Callable<String> {
	Tree t1, t2;
	public Task(Tree t1, Tree t2) {
		this.t1 = t1;
		this.t2 = t2;
	}
	
	@Override
	public String call() throws Exception {
		AStarSearchDiff gd = new AStarSearchDiff(t1, t2);
    	gd.calculate();
    	for(AbstractGraphDiff.DiffResult dr : gd.getDiff()) {
    		System.out.println(dr.toString());
    	}
    	System.out.println(gd.getTransform());
		return null;
	}

}
