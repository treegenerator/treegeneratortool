package treegenerator.algorithms;

import java.util.ArrayList;
import java.util.List;

import classhier.graph.Node;
import classhier.graph.Tree;
import classhier.graph.TreeNode;

public class GeneratePattern extends AGenerateAlgorithm{
	private int depth;  
	private float dist[];
	
	public GeneratePattern(GenParams gp) {
		super(gp);
		this.dist = gp.getDist();
		this.depth = this.dist.length;
	}

	@Override
	public Tree generate() {
		//Tree1
		this.genTree = new Tree();
		
		//create nodes
		for(int i = 0; i < nodesSize; i++) {
			TreeNode n = new TreeNode(this.getNewSymbolicName());
			this.genTree.addNode(n);
		}
		
		List<Node> nodes = new ArrayList<>(this.genTree.getNodes().values());		
		List<Node> prevLev = new ArrayList<Node>();

		Node root = nodes.get(
				rand.nextInt(nodes.size()));
		this.genTree.setRoot((TreeNode)root);
		nodes.remove(root);
		prevLev.add(root);
		
		//get no of nodes per depth/level
		int[] ns = null;
		try {
			ns = this.getNoOfNodesPerDepth(dist);
		} catch(Exception e) {
			System.out.println(e);
			return null;
		}
		
		//for each level
		for(int l = 0; l < dist.length; l++) {
			List<Node> tmpPrevLev = new ArrayList<Node>();
			for(int j = 0; j < ns[l]; j++) {
				Node n = nodes.get(rand.nextInt(nodes.size()));
				Node p = null;
				try {
					p = prevLev.get(rand.nextInt(prevLev.size()));
				} catch(Exception ex) {
					System.out.println(ex.toString());
				}
			
				try {
					this.genTree.newEdge(p, n);
				}catch(Exception ex) {
					System.out.println(ex.toString());
				}
				//add to prev lev
				tmpPrevLev.add(n);
				//remove from unused
				nodes.remove(n);
			}
			prevLev.clear();
			prevLev = tmpPrevLev;
			//randomly connect to previous level nodes
			//used node remove from unused nodes and add them to prev level nodes
		}
		this.genTree.performBreadthFirstTraversal();
		
		return this.genTree;
	}
	
	private int[] getNoOfNodesPerDepth(float...dist) throws Exception {
		int[] noNodes = new int[dist.length];
		int cnt = 0;
		
		for(int i = 0; i < dist.length; i++) {
			int n = (int)(dist[i] * (this.nodesSize - 1)); //we are using floor value, without root node!
			noNodes[i] += n;
			cnt += n;
		}
		//if we have less nodes than n1, then we randomly set the rest of nodes per levels
		if(cnt < this.nodesSize) {
			int diff = this.nodesSize - 1 - cnt;
			
			if(diff > 0) {
				//if diff is larger than number of levels, set to each level equal number of nodes
				if(diff > dist.length) { //depth
					int n = diff / dist.length; //again floor it
					for(int i = 0; i < dist.length; i++) {
						noNodes[i] += n;
						cnt += n;
					}
					//calculate again for case when there is less than number of levels
					diff = this.nodesSize - 1 - cnt;
				}
				
				//diff is less than number of levels
				if(diff < dist.length) {
					for(int i = 0; i < noNodes.length && diff > 0; i++) {
						if(noNodes[i] == 0) {
							noNodes[i]++;
							diff--;
							cnt++;
						}
					}
					
					if(diff > 0) {
						//random select diff number of levels and add nodes to them
						for(int i = 0; i < diff; i++) {
							int j = rand.nextInt(dist.length);
							noNodes[j] += 1;
							cnt++;
						}
					}
				}
				
			}
		}
		
		if(cnt != (this.nodesSize - 1)) {
			throw new Exception("Cnt and nodes size mismatch, something is wrong!");
		}
		
		return noNodes;
	}
}
