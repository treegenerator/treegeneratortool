package treegenerator.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import classhier.graph.Node;
import classhier.graph.Tree;

public abstract class AGenerateAlgorithm {
	public final boolean verbose = false;
	protected final char beginLetter = 'A';
	protected final char endLetter = 'Z';
	
	protected String lastSymb = String.valueOf(beginLetter);
	protected Tree genTree;
	protected List<Tree> steps;
	protected int nodesSize, nodeSize2;
	protected GenParams genParams;
	
	public static Random rand;
	
	public AGenerateAlgorithm(GenParams gp) {
		this.genParams = gp;
		this.nodesSize = gp.getSize();
		this.rand = new Random();
	}
	
	public abstract Tree generate();
	
	public Tree getTree() {
		return this.genTree;
	}
	
	protected void findLastSymbolicName(Tree t) {
		List<Node> nodes = new ArrayList<Node>();
		nodes.addAll(t.getNodes().values());
		Collections.sort(nodes, Collections.reverseOrder());
		this.lastSymb = nodes.get(0).getSymbolicName();
	}
	
	protected String getNewSymbolicName() {
		int number = -1;
		char letter = this.lastSymb.charAt(0);
		
		if(this.lastSymb.length() > 1) {
			number = Integer.parseInt(this.lastSymb.substring(1));
		}
		
		if(letter == endLetter) {
			letter = beginLetter;
			if(number == -1) {
				number = 1;
			} else {
				number++;
			}
		} else {
			letter++;
		}
		
		if(number != -1) {
			this.lastSymb = letter + String.valueOf(number);
		} else {
			this.lastSymb = String.valueOf(letter);
		}
		return this.lastSymb;
	}
}
