package treegenerator.algorithms;

import classhier.graph.Tree;
import javafx.util.Pair;
import java.util.Random;

public class TreeComparisonGen {
	Tree t1, t2;
	
	public TreeComparisonGen() {
	}
	
	public Pair<Tree, Tree> generateRandomDistort(GenParams params) throws Exception{
		t1 = new GenerateRandom(params).generate();
		params.setInputTree(t1);
		t2 = new DistortTree(params).generate();
		return new Pair<Tree, Tree>(t1, t2);
	}
	
	public Pair<Tree, Tree> generatePatternDistort(GenParams params) throws Exception{
		t1 = new GeneratePattern(params).generate();
		params.setInputTree(t1);
		t2 = new DistortTree(params).generate();
		return new Pair<Tree, Tree>(t1, t2);
	}
	
	public Pair<Tree, Tree> getTrees(){
		return new Pair<Tree, Tree>(t1, t2);
	}
	
	public Pair<Tree, Tree> generateRandomDistort(int size, int addedNodes, int deletedNodes, float matchedPermutation){
		GenParams gp = new GenParams(size, addedNodes, deletedNodes, matchedPermutation);
		t1 = new GenerateRandom(gp).generate();
		gp.setInputTree(t1);
		t2 = new DistortTree(gp).generate();
		return new Pair<Tree, Tree>(t1, t2);
	}
	
	public Tree distort(Tree t1, int addedNodes, int deletedNodes, float matchedPermutation) {
		this.t1 = t1;
		GenParams gp = new GenParams(t1, addedNodes, deletedNodes, matchedPermutation);
		this.t2 = new DistortTree(gp).generate();
		return t2;
	}

}
