package treegenerator.algorithms;

import java.util.ArrayList;
import java.util.List;

import classhier.graph.Node;
import classhier.graph.Tree;
import classhier.graph.TreeNode;

public class GenerateRandom extends AGenerateAlgorithm {
	private int equals_hit = 0;
	
	public GenerateRandom(GenParams gp) {
		super(gp);
	}
	
	@Override
	public Tree generate() {
		return this.generateTreePCUn();
	}

	//random P-C variant
	public Tree generateRandomPC() {
		//Tree
		this.genTree = new Tree();
		this.nodesSize = this.genParams.getSize();
		
		if(this.genParams.useSteps()) {
			this.steps = new ArrayList<Tree>();
		}
		
		//create nodes
		for(int i = 0; i < this.nodesSize; i++) {
			TreeNode n = new TreeNode(this.getNewSymbolicName());
			genTree.addNode(n);
		}
		
		//add step tree
		if(this.genParams.useSteps()) {
			steps.add(genTree);
		}
		
		TreeNode[] nodes1 = genTree.getNodes().values().toArray(new TreeNode[nodesSize]);
		
		int edges = 0;
		this.equals_hit = 0;
		do {
			//generate random edges
			//get two random integers from 0 to nodesSize1
			int i1 = rand.nextInt(nodesSize),
					i2 = rand.nextInt(nodesSize);
			if(i1 == i2) {
				this.equals_hit++;
				continue;
			}
			TreeNode n1 = nodes1[i1], n2 = nodes1[i2];
			
			//check if nodes contain parents
			if(n1.hasParent() && n2.hasParent()) {
				continue;
			}
			
			//check nodes already connected
			ArrayList<Node> n1preds = new ArrayList<Node>(),
						n2preds = new ArrayList<Node>();
			n1.getPreds(n1preds); n2.getPreds(n2preds);
			if(n1preds.contains(n2) || n2preds.contains(n1)) {
				continue;
			}
			
			//first is parent, second is child candidate
			//check second has no current parent, if not first is parent
			if(!n2.hasParent()) {
				genTree.newEdge(n1, n2);
				edges++;
				//add step tree
				if(this.genParams.useSteps()) {
					steps.add(genTree);
				}
			} else if(!n1.hasParent()) {
				genTree.newEdge(n2, n1);
				edges++;
				//add step tree
				if(this.genParams.useSteps()) {
					steps.add(genTree);
				}
			} else {
				continue;
			}
			//otherwise check that first has no parent, if not second is parent
			//else try with another random integer pair
			//repeat until all nodes are connected, number of edges equals to nodesSize1 - 1
		} while(edges < nodesSize - 1);
		
		//find root
		genTree.findRoot();
		
		return genTree;

	}
	
	//random P-C unused set
	public Tree generateTreePCUn() {
		//Tree
		this.genTree = new Tree();
		this.nodesSize = this.genParams.getSize();
		
		if(this.genParams.useSteps()) {
			this.steps = new ArrayList<Tree>();
		}
		
		//create nodes
		for(int i = 0; i < nodesSize; i++) {
			TreeNode n = new TreeNode(this.getNewSymbolicName());
			this.genTree.addNode(n);
		}
		
		//add step tree
		if(this.genParams.useSteps()) {
			steps.add(genTree);
		}
		
		List<TreeNode> nodes = new ArrayList(this.genTree.getNodes().values());
		List<TreeNode> single = new ArrayList(this.genTree.getNodes().values());
		
		int edges = 0;
		do {
			//generate random edges
			//get two random integers from 0 to nodesSize1
			int i1 = rand.nextInt(nodesSize),
					i2 = rand.nextInt(single.size());
			
			TreeNode n1 = (TreeNode)nodes.get(i1), n2 = (TreeNode)single.get(i2);
			if(n1.equals(n2)) {
				equals_hit++;		
				continue;			
			}
			
			//check nodes already connected
			ArrayList<Node> n1preds = new ArrayList<Node>();
			n1.getPreds(n1preds);
			
			if(n1preds.contains(n2)) {
				continue;
			}
			this.genTree.newEdge(n1, n2);
			edges++;
			single.remove(n2);
			
			//add step tree
			if(this.genParams.useSteps()) {
				steps.add(genTree);
			}
			
			//otherwise check that first has no parent, if not second is parent
			//else try with another random integer pair
			//repeat until all nodes are connected, number of edges equals to nodesSize1 - 1
		} while(edges < nodesSize - 1);
		
		//find root
		this.genTree.findRoot();
		return this.genTree;
	}
	
	//iterative child random parent
	public Tree generateTreeIterChild() {
		//Tree1
		this.genTree = new Tree();
		this.nodesSize = this.genParams.getSize();
		
		if(this.genParams.useSteps()) {
			this.steps = new ArrayList<Tree>();
		}
		
		//create nodes
		for(int i = 0; i < nodesSize; i++) {
			TreeNode n = new TreeNode(this.getNewSymbolicName());
			this.genTree.addNode(n);
		}
		
		List<TreeNode> nodes = new ArrayList(this.genTree.getNodes().values());
		
		//add step tree
		if(this.genParams.useSteps()) {
			steps.add(genTree);
		}
		
		int edges = 0;
		//skip random?
		int ri = rand.nextInt(nodesSize);
		for(int si = 0; si < nodes.size(); si++) {
			if(si == ri) {
				continue;
			}
			TreeNode s = nodes.get(si);
			do {
				//generate random edges
				int i = rand.nextInt(nodesSize);
				TreeNode n = (TreeNode)nodes.get(i);
				if(n.equals(s)) {
					equals_hit++;
					continue;
				}
				
				//check nodes already connected
				ArrayList<Node> npreds = new ArrayList<Node>();
				
				n.getPreds(npreds);
				if(npreds.contains(s)) {
					continue;
				}
				
				//first is parent, second is child candidate
				//check second has no current parent, if not first is parent
				this.genTree.newEdge(n, s);
				edges++;
				
				//add step tree
				if(this.genParams.useSteps()) {
					steps.add(genTree);
				}
				break;
				//otherwise check that first has no parent, if not second is parent
				//else try with another random integer pair
				//repeat until all nodes are connected, number of edges equals to nodesSize1 - 1
			} while(true);
		}
		
		
		this.genTree.findRoot();
		return this.genTree;
	}
	
	//random p-c used/unused set
	public Tree generateTree1_RandomUsedUnused() {
		//Tree1
		this.genTree = new Tree();
		this.nodesSize = this.genParams.getSize();
		
		if(this.genParams.useSteps()) {
			this.steps = new ArrayList<Tree>();
		}
		
		//create nodes
		for(int i = 0; i < nodesSize; i++) {
			TreeNode n = new TreeNode(this.getNewSymbolicName());
			this.genTree.addNode(n);
		}
		
		//add step tree
		if(this.genParams.useSteps()) {
			steps.add(genTree);
		}

		//random select child and random select used as parent
		List<TreeNode> used = new ArrayList();
		List<TreeNode> unused = new ArrayList(this.genTree.getNodes().values());
		
		//random select root add to used
		int ri = rand.nextInt(unused.size());
		TreeNode r = unused.get(ri);
		this.genTree.setRoot(r);
		used.add(r);
		unused.remove(r);
		
		int edges = 0;
		do {
			//generate random edges
			//get two random integers from 0 to nodesSize1
			int i1 = rand.nextInt(used.size()),
					i2 = rand.nextInt(unused.size());

			TreeNode n1 = used.get(i1), n2 = unused.get(i2);
		
			this.genTree.newEdge(n1, n2);
			edges++;
			unused.remove(n2);
			used.add(n2); 
			
			//add step tree
			if(this.genParams.useSteps()) {
				steps.add(genTree);
			}
			//otherwise check that first has no parent, if not second is parent
			//else try with another random integer pair
			//repeat until all nodes are connected, number of edges equals to nodesSize1 - 1
		} while(edges < nodesSize - 1);
		return genTree;
	}

	public int getEqualsHit() {
		return this.equals_hit;
	}
	
	public void resetEqualsHit() {
		this.equals_hit = 0;
	}
}
