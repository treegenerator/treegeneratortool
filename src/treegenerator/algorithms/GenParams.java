package treegenerator.algorithms;

import classhier.graph.Tree;

public class GenParams {
	private int size, addedNodes, deletedNodes;
	private float matchedPermutation;
	private float[] dist;
	private boolean steps;
	private Tree inputTree;
	
	public GenParams(int size, int addedNodes, int deletedNodes, float matchedPermutation) {
		this(size, addedNodes, deletedNodes, matchedPermutation, false);
	}
	
	public GenParams(int size, int addedNodes, int deletedNodes, float matchedPermutation, boolean steps) {
		this.size = size;
		this.addedNodes = addedNodes;
		this.deletedNodes = deletedNodes;
		this.matchedPermutation = matchedPermutation;
	}
	
	public GenParams(int size, int addedNodes, int deletedNodes, float matchedPermutation, float[] dist, boolean steps) {
		this(size, addedNodes, deletedNodes, matchedPermutation, steps);
		this.dist = dist;
	}
	
	public GenParams(int size, float[] dist) {
		this.size = size;
		this.dist = dist;
	}
	
	public GenParams(Tree t1, int addedNodes, int deletedNodes, float matchedPermutation) {
		this.inputTree = t1;
		this.size = t1.getSize();
		this.addedNodes = addedNodes;
		this.deletedNodes = deletedNodes;
		this.matchedPermutation = matchedPermutation;
	}

	public GenParams(int size) {
		this.size = size;
	}

	public int getSize() {
		return this.size;
	}
	
	public int getAddedNodes() {
		return this.addedNodes;
	}
	
	public int getDeletedNodes() {
		return this.deletedNodes;
	}
	
	public float getMatchedPermutation() {
		return this.matchedPermutation;
	}
	
	public float[] getDist() {
		return this.dist;
	}
	
	public void setDist(float[] dist) {
		this.dist = dist;
	}
	
	public boolean useSteps() {
		return this.steps;
	}
	
	public void setInputTree(Tree t) {
		this.inputTree = t;
	}

	public Tree getInputTree() {
		return this.inputTree;
	}
	
	public String toString() {
		return String.format("size: %d, added: %d, deleted: %d, matchperm: %.2f",
				size, addedNodes, deletedNodes, matchedPermutation);
	}
}
