package treegenerator.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import classhier.graph.Node;
import classhier.graph.Tree;
import classhier.graph.TreeNode;

public class DistortTree extends AGenerateAlgorithm {
	private Tree inputTree;
	List<TreeNode> removedNodes = new ArrayList<TreeNode>();
	List<TreeNode> newNodes = new ArrayList<TreeNode>();
	List<TreeNode> matchedNodes = new ArrayList<TreeNode>();
	
	private int nodesSize1, nodeSize2;
	private int addedNodes = 10, deletedNodes = 10;
	private float edgePermutationFactor = 0.5f; // percentage of changed parents
	
	public DistortTree(GenParams gp) {
		super(gp);
		this.addedNodes = gp.getAddedNodes();
		this.deletedNodes = gp.getDeletedNodes();
		this.edgePermutationFactor = gp.getMatchedPermutation();
		this.nodesSize1 = gp.getSize();
		this.inputTree = gp.getInputTree();
		this.findLastSymbolicName(this.inputTree);
	}
	
	@Override
	public Tree generate() {
		//clone entire tree
		this.genTree = (Tree)this.inputTree.clone();
		
		//create array
		TreeNode[] nodes1 = this.genTree.getNodes().values().toArray(new TreeNode[nodesSize]);

		matchedNodes = new ArrayList<TreeNode>(Arrays.asList(nodes1));
		for(int i = 0, size = nodesSize; i < deletedNodes; i++, size--) {
			int dindex = rand.nextInt(size);
			TreeNode d1 = matchedNodes.remove(dindex);
			this.genTree.removeNode(d1.getName(), true);
			
			removedNodes.add(d1);
			if(verbose)
				System.out.println("remove: " + d1.getName() + " ==> λ");
		
		}
		
		int t2size = this.genTree.getSize();
		for(int i = 0; i < addedNodes; i++) {
			//create node
			TreeNode n = (TreeNode)this.genTree.newNode(this.getNewSymbolicName());
			
			//if there is no nodes in t2 set root
			if(t2size == 0) {
				this.genTree.setRoot(n);
			// else add to random parent 
			} else {
				List<Node> nodes = new ArrayList<>(this.genTree.getNodes().values());
				nodes.remove(n); //remove newly added node, to avoid circular connection
				Node parent = nodes.get(rand.nextInt(nodes.size()));
			
				//random select to replace one child or to create new child
				Node c = null;
				
				if(parent.hasChildren() && rand.nextInt(2) == 1) {
					Node[] children = parent.getChildren().toArray(new Node[parent.getChildren().size()]);
					int cindex = rand.nextInt(children.length);
					c = children[cindex];
				}
				
				if(c != null) {
					if(verbose)
						System.out.println("insert-replace: " + c.getName() + " ==> " + n.getName());
					this.genTree.insertNode(n, c);
				} else {
					if(verbose)
						System.out.println("insert-add: λ ==> " + n.getName());
					this.genTree.newEdge(parent, n);
				}
			}
			
			t2size++;
			newNodes.add(n);
		}
		
		int matchedSize = matchedNodes.size();
		int nodesForChange = Math.round(matchedSize * edgePermutationFactor);
		int changed = 0;
		ArrayList<Node> unused = new ArrayList<Node>(this.matchedNodes);
		List<Node> nodes = new ArrayList<>(this.genTree.getNodes().values());
		while(changed < nodesForChange) {
			//random select matched node in v2 to change edge to random selected parent in v2
			int ni = rand.nextInt(unused.size()),
					pi = rand.nextInt(nodes.size());

			//get node
			Node n = this.genTree.getNode(unused.get(ni).getName());
			//get parent
			Node parent = this.genTree.getNode(nodes.get(pi).getName());
			
			//check if selected nodes are the same node
			if(n.getName().equals(parent.getName()) || n.getParents().contains(parent)) { 
				continue;
			}
		
			//random select to replace one child or to create new child
			//or equal distribution for decision to create soley new child or to insert child
			Node c = null;
			if(parent.hasChildren() && rand.nextInt(2) == 1) {
				Node[] children = parent.getChildren().toArray(new Node[parent.getChildren().size()]);
				int cindex = rand.nextInt(children.length);
				c = children[cindex];
				
				if(c != null && c.getName().equals(n.getName())) {
					continue;
				}
			}

			
			//remove node n
			this.genTree.removeNode(n.getName(), false);
			
			if(c != null) {
				if(verbose)
					System.out.println("matched-swap: " + n.getName() + " ==> " + c.getName());
				this.genTree.insertNode(n.getName(), c);
			} else {
				this.genTree.newEdge(parent, n);
				if(verbose)
					System.out.println("matched-insert: " + parent.getName() + " ==> " + n.getName());
			}
			unused.remove(n);
			changed++;		
			
		} 
		
		//find root
		this.genTree.findRoot();
		
		return this.genTree;
	}
}
